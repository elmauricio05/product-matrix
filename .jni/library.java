/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
	//@SuppressWarnings("unused")
	//private Integer productRows;
	//@SuppressWarnings("unused")
	//private Integer productColumns;

    @executerpane.MethodAnnotation(signature = "product(int**,int,int,int**,int,int,int*,int*):int**")
    public int[][] product(int[][] left, int leftRows, int leftColumns, int[][] right, int rightRows, int rightColumns, Integer productRows, Integer productColumns){
        if (productRows == null){
            throw new IllegalArgumentException("expected productRows index");
        }
        if (productColumns == null){
            throw new IllegalArgumentException("expected productColumns index");
        }
        return product_(left, leftRows, leftColumns, right, rightRows, rightColumns, productRows, productColumns);
        //int[][] kk = product_(left, leftRows, leftColumns, right, rightRows, rightColumns, productRows, productColumns);
        //productRows = this.productRows;
        //productColumns = this.productColumns;
        //return kk;
    }
    private native int[][] product_(int[][] left, int leftRows, int leftColumns, int[][] right, int rightRows, int rightColumns, Integer productRows, Integer productColumns);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
